;;; org-tw.el --- taskwarrior org mode bridge        -*- lexical-binding: t; -*-

;; Copyright (C) 2020  Sebastian Fieber

;; Author: Sebastian Fieber <fallchildren@disroot.org>
;; Keywords: convenience, tools

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;; This is an implementation of my workflow using org-mode and
;; taskwarrior which may or may not fit anyone but me.
;;
;; Description of my workflow:
;;
;; TODO

;;; Code:

(require 'cl-lib)
(require 'json)
(require 'ol)
(require 'org-id)
(require 'org-capture)
(require 'simple)
(require 'subr-x)

;; custom
(defgroup org-tw nil
  "Org-mode taskwarrior bridge.

Create taskwarrior tasks and accompanying org files for them."
  :group 'external
  :group 'convenience)

(defcustom org-tw-task-executable "task"
  "Path to the taskwarrior executable to be used."
  :group 'org-tw
  :type 'file)

(defcustom org-tw-priorities '(("high" . "H")
                               ("medium" . "M")
                               ("low" . "L")
                               ("none" . nil))
  "Priorities configured for taskwarrior."
  :group 'org-tw
  :type '(alist :key-type string :value-type string))

(defcustom org-tw-file  "~/.emacs.d/task/tw.org"
  "Directoy where org-tw will put org files for tasks."
  :group 'org-tw
  :type 'file)

(defcustom org-tw-parse-capture-template-fn 'org-tw--parse-capture-template
  "Function used for parsing `org-tw-capture-template'.

The result should be a task structure which can be fed to
`org-tw--build-command'."
  :group 'org-tw
  :type 'function)

(defcustom org-tw-before-create-task-hook nil
  "Hook run before org-file for task is created."
  :group 'org-tw
  :type 'hook)

(defcustom org-tw-after-create-task-hook nil
  "Hook run after org-file for task is created."
  :group 'org-tw
  :type 'hook)

(defvar org-tw-capture-template
  `("t" "org-tw task template" entry (file ,org-tw-file)
    ,(format "* TODO %%? %%^g
  :PROPERTIES:
  :TASKWARRIOR_PRIORITY: %%^{Priority%s}
  :TASKWARRIOR_DUE: %%^t
  :END:

  %%i

  %%a" (concat "|"
               (cl-reduce (lambda (x y) (concat x "|" y)) (mapcar #'car org-tw-priorities))))
    :prepend t)
  "Capture template used for interactive task creation.

See `org-capture-templates' for general information about capture
templates.

Used by `org-tw-create-task'.  Make sure to also customize
`org-tw-parse-capture-template-fn' to fit any new value.")

(defvar org-tw-import-capture-template
  `("T" "org-tw task import template" entry (file ,org-tw-file))
  "Part of the capture template for importing.

`org-tw-import-all-tasks-without-org-file' will use this,
`org-tw-import-capture-template-body' and
`org-tw-import-capture-template-properties' to generate a fitting
capture template for importing.")

(defvar org-tw-import-capture-template-body "* TODO %s %s
  :PROPERTIES:
  :TASKWARRIOR_PRIORITY: %s
  :TASKWARRIOR_DUE: %s
  :ID:       %s
  :END:

  I was automatically created. Change me.
"
  "The body part of the capture template for importing.

`org-tw-import-all-tasks-without-org-file' will use this,
`org-tw-import-capture-template' and
`org-tw-import-capture-template-properties' to generate a fitting
capture template for importing.")

(defvar org-tw-import-capture-template-properites '(:prepend t)
  "The body part of the capture template for importing.

`org-tw-import-all-tasks-without-org-file' will use this,
`org-tw-import-capture-template' and
`org-tw-import-capture-template-body' to generate a fitting
capture template for importing.

Note that the :immediate-finish property is always added to the
resulting template by `org-tw--get-import-capture-template'.

See `org-capture-templates' for more infos about properties of
org-capture.")

(defvar org-tw-read-arguments-fn-alist
  '(("description" . org-tw--prepare-plain)
    ("project" . org-tw--prepare-default)
    ("priority" . org-tw--prepare-default)
    ("due" . org-tw--prepare-default)
    ("tags" . org-tw--prepare-tags))
  "Mapping of taskwarrior arguments to read and prepare functions.

The first member of each entry should be the name of the
argument.  The second member is the read function.  The third
member is the prepare function.

A read function should accept no arguments.

A prepare function should accept two values: the first one is the
name of the argument while the second one is the return value of
the prepare function.  The prepare function should a string which
will be added to the task command.")



;; internal functions
(defun org-tw--get-prepare-fn (arg fn-alist)
  "Gets the prepare function from FN-ALIST.

ARG is a string which should be contained as the car value of one
of the lists inside FN-ALIST.

FN-ALIST should be or at least look like
`org-tw-read-arguments-fn-alist'."
  (cdr (assoc arg fn-alist)))

(defun org-tw--prepare-default (name arg)
  "Construct a default argument for taskwarrior.

NAME is the name of the argument e.g. priority.
ARG is the value this argument should be given.

If arg is nil or empty this function will return nil.

e.g. priority h -> priority:h"
  (unless (or (null arg)
              (string-empty-p arg))
    (concat name ":" arg)))

(defun org-tw--prepare-plain (name arg)
  "Check if ARG is nil or empty and return eiter nil or ARG.

Argument NAME is not used."
  (unless (or (null arg)
              (string-empty-p arg))
    arg))

(defun org-tw--read-description ()
  "Ask for the description of a task."
  (concat "\""
          (read-string "Description: ")
          "\""))

(defun org-tw--read-project ()
  "Ask for the project of a task."
  (read-string "Project: "))

(defun org-tw--read-tags ()
  "Ask for the tags of a task.

Tags maybe comma or space seperated."
  (read-string "Tags: "))

(defun org-tw--prepare-tags (name arg)
  "Prepare tags for usage in the task command.

Return a list of proper taskwarrior tasks ('+' appended).

e.g. \"tag1, tag2 tag3\" -> '(\"+tag1\" \"+tag2\" \"+tag3\")

Argument NAME is not used.
Argument ARG is the string containing the tags."
  (string-join (mapcar (lambda (x) (concat "+" x))
                       arg) " "))


;; interacting with taskwarrior json export
(defun org-tw--task-hash-to-read-values (task)
  (cl-remove-if (lambda (x) (null (cdr x)))
                `(("id" . ,(gethash "id" task))
                  ("uuid" . ,(gethash "uuid" task))
                  ("description" . ,(gethash "description" task))
                  ("project" . ,(gethash "project" task))
                  ;; turn vector to list.. there must be some better way
                  ("tags" . ,(mapcar (lambda (x) x) (gethash "tags" task)))
                  ("priority" . ,(gethash "priority" task))
                  ("due" . ,(gethash "due" task)))))

(defun org-tw--get-task-from-filter (filter)
  "Construct alist of taskwarrior task which matches FILTER."
  (let ((json-export (json-parse-string (shell-command-to-string (format "task %s export"
                                                                         filter)))))
    (when (> (length json-export) 0)
      (org-tw--task-hash-to-read-values
       (aref json-export
             0)))))

(defun org-tw--get-property (task property)
  "Get PROPERTY from a TASK alist."
  (cdr (assoc property task)))


;; command creation functions
(defun org-tw--build-command (read-values)
  "Build the taskwarrior command.

READ-VALUES should be an alist looking like this:

    '((ARGUMENT . VALUE)
      (ARGUMENT . VALUE)
      ...)

Each VALUE will be processed with a prepare function from
`org-tw-read-arguments-fn-alist' and then will be concated to the
taskwarrior command.  Make sure ARGUMENT has an entry in
`org-tw-read-arguments-fn-alist'."
  (let* ((res (concat org-tw-task-executable " add ")) )
    (if (null (assoc "description" read-values))
        (error "No description given")
      (dolist (entry read-values res)
        (unless (null (cdr entry))
          (setq res (concat res
                            (funcall (org-tw--get-prepare-fn (car entry)
                                                             org-tw-read-arguments-fn-alist)
                                     (car entry)
                                     (cdr entry))
                            " "))))
      (string-trim res))))

;; org-capture related stuff
(defun org-tw--convert-to-org-tags (task)
  "Convert specific values of TASK to tags.
ATM project and tags are hardcoded"
  (let* ((tags (org-tw--get-property task "tags") )
         (project (org-tw--get-property task "project") )
         (org-tags (cl-remove-if #'null (cons project tags))))
    (if org-tags
        (concat ":"
                (string-join org-tags ":")
                ":")
      "")))

(defun org-tw--get-import-capture-template (task)
  (append org-tw-import-capture-template
          (format org-tw-import-capture-template-body
                  (cdr (assoc "description" task))
                  (org-tw--convert-to-org-tags task)
                  (cdr (rassoc (cdr (assoc "priority" task) org-tw-priorities)))
                  (if (cdr (assoc "due" task))
                      (with-temp-buffer (org-insert-time-stamp
                                         (date-to-time (cdr (assoc "due" task))))
                                        (buffer-string))
                    "nil")
                  (cdr (assoc "uuid" task)))
          (append '(:immediate-finish t) org-tw-import-capture-template-properites)))

(defun org-tw--parse-capture-template ()
  (save-excursion
    (save-match-data
      (org-narrow-to-subtree)
      (let* ((all-tags (car (org-element-map (org-element-parse-buffer) 'headline
                              (lambda (headline) (mapcar #'substring-no-properties
                                                    (org-element-property :tags headline))))))
             (project (car all-tags))
             (tags (cdr all-tags)))
        `(("description" . ,(concat "\""
                                    (replace-regexp-in-string
                                     "\"" "\\\\\""
                                     (car (org-element-map (org-element-parse-buffer) 'headline
                                            (lambda (headline) (org-element-property :raw-value headline)))))
                                    "\""))
          ("project" . ,project)
          ("priority" . ,(let ((priority (org-entry-get nil "TASKWARRIOR_PRIORITY")))
                           (when (some (lambda (x) (string= x priority)) (mapcar #'car org-tw-priorities))
                             (cdr (assoc priority org-tw-priorities)))))
          ("due" . ,(let ((due (org-entry-get nil "TASKWARRIOR_DUE")))
                      (and due
                           (string-match "\\([0-9]+-[0-9]+-[0-9]+\\)" due)
                           (match-string 1 due))))
          ("tags" . ,tags))))))

(defun org-tw--capture-before-finalize-hook-fn ()
  (let ((key (plist-get org-capture-plist :key))
        (desc (plist-get org-capture-plist :description)))
    (when (and (string= desc (cadr org-tw-capture-template))
               (string= key (car org-tw-capture-template)))
      (let* ((command (org-tw--build-command
                       (funcall org-tw-parse-capture-template-fn)))
             (result (string-trim (shell-command-to-string command)))
             (id (save-match-data
                   (string-match "[0-9]+" result)
                   (substring-no-properties result
                                            (match-beginning 0)
                                            (match-end 0))))
             (task (org-tw--get-task-from-filter (concat "id:" id)))
             (uuid (org-tw--get-property task "uuid")))
        (org-set-property "ID" uuid)))))

;; org specific
(defun org-tw-after-todo-state-change-hook-fn ()
  (when (and (string-match-p (expand-file-name (file-name-directory org-tw-file)) (buffer-file-name))
             ;; we don't want to touch recurring tasks, or do we?
             (not (org-entry-get nil "LAST_REPEAT")))
    (let* ((uuid (org-entry-get nil "ID"))
           (filter (concat "uuid:" uuid))
           (task (org-tw--get-task-from-filter filter))
           (cmd-format-str (cond ((equal org-state "DONE") "%s %s done")
                                 ((equal org-state "TODO") "%s %s modify end: status:pending")
                                 ;; do nothing if not DONE or TODO
                                 (t (setq task nil)))))
      (when task
        (with-current-buffer (get-buffer-create "*taskwarrior*")
          (insert (shell-command-to-string (format cmd-format-str
                                                   org-tw-task-executable
                                                   uuid))
                  "\n"))))))


;; additional helpers for cli call of emacsclient
(defun org-tw-create-org-task-from-task (task)
  "Creates an org-file for alist TASK."
  (let ((org-capture-templates `(,(org-tw--get-import-capture-template task))))
        (run-hooks 'org-tw-before-create-task-hook)
        (org-capture nil "T")
        (run-hooks 'org-tw-after-create-task-hook)))

(defun org-tw-maybe-create-and-goto-task (uuid)
  "Check if task with UUID needs an org task and jump to its position.

Primarily designed to be called from the command line."
  (if (org-id-find-id-in-file uuid org-tw-file)
      (progn (find-file org-tw-file)
             (widen)
             (org-id-goto uuid))
    (let ((task (org-tw--get-task-from-filter (concat "uuid:" uuid))))
      (org-tw-create-org-task-from-task task)
      (find-file org-tw-file)
      (let ((marker (org-find-exact-headline-in-buffer
                     (org-tw--get-property task "description"))))
        (goto-char (marker-position marker))))))

;;;###autoload
(defun org-tw-cli-get-task-link (uuid)
  (save-window-excursion
    (save-excursion
      (org-tw-maybe-create-and-goto-task uuid)
      (if (org-entry-get nil "TASKWARRIOR_OPEN")
          (org-entry-get nil "TASKWARRIOR_OPEN")
        ""))))

;;;###autoload
(defun org-tw-cli-open-task (uuid)
  (select-frame-set-input-focus (selected-frame))
  (org-tw-maybe-create-and-goto-task uuid)
  (org-show-subtree))

;;;###autoload
(defun org-tw-clock-in (id uuid)
  "Clock into task with ID.

Primarily designed to be called from the command line."
  (save-window-excursion
    (save-excursion
      (org-tw-maybe-create-and-goto-task uuid)
      (org-clock-in)
      (unless (and (org-entry-get nil "TASKWARRIOR_NO_START")
                   (string= "t" (org-entry-get nil "TASKWARRIOR_NO_START")))
        (with-current-buffer (get-buffer-create "*taskwarrior*")
          (insert (shell-command-to-string (format "%s %s start"
                                                   org-tw-task-executable
                                                   id))
                  "\n"))))))



;; interactive functions
;;;###autoload
(defun org-tw-import-all-tasks-without-org-file ()
  "Create org-files for all pending tasks which have none."
  (interactive)
  (let* ((json-export (json-parse-string (shell-command-to-string "task status:pending export")))
         (tasks (mapcar #'org-tw--task-hash-to-read-values json-export)))
    (mapc (lambda (task) (org-tw-create-org-task-from-task task)) tasks)))

;;;###autoload
(defun org-tw-create-task ()
  "Create a task in `org-tw-file'.

The arguments to the taskwarrior executable can be configured
with `org-tw-read-arguments-list'.  Also priorities may be
configured via `org-tw-priorities'.

Templates for the created org file can be set via
`org-tw-file-template' and `org-tw-file-template-with-link'.  The
second one is chosen if the user accepted to store an org-link
when calling this function."
  (interactive)
  (let ((org-capture-templates `(,org-tw-capture-template)))
    (run-hooks 'org-tw-before-create-task-hook)
    (org-capture nil "t")
    (run-hooks 'org-tw-after-create-task-hook)))

;;;###autoload
(defun org-tw-setup ()
  "Setup org-tw."
  (interactive)
  (add-hook 'org-capture-before-finalize-hook #'org-tw--capture-before-finalize-hook-fn)
  (add-hook 'org-after-todo-state-change-hook #'org-tw-after-todo-state-change-hook-fn))

(provide 'org-tw)
;;; org-tw.el ends here
